# my.configs

Hello. This is my dotfiles.

## Colors

In colors I put some Xdefault colors that I liked. Most of them I found at
terminal sexy and changed a little.

## i3

### Init

At the begging of the config file is setted these vars:

    $mod           mod4               Which means that the main key is Win key
    $term          urxvt              The terminal used is urxvt
    $menu          dmenu_run          The menu to execute commands or open
                                      applications. ($mod + d)
    $lock          my.lock            The script/application to run for lock
    $bar_restart   my.bar restart     The script/application to restart the bar
    $x_restart     my.x restart       The script/application to restart the x
    $bg            feh                The background to run at i3 startup.
    $vol_down/up   mixer vol +/-      The mixer to inc/dec audio volume

And some others related to transparency, which you can use the scripts available
here as well. (transset dependency!)

Within these vars are the dependencies (and maybe with others forgoten).

### How it works

My i3 configs works mainly with modes. You call a mode, the polybar will update
so you can se an hint at the bar (which is the mode name) and then it will be
locked there until you press q or other key that returns to the default mode.

Some keys:

    Keys            Name            Options
    $mod+Shift+A    App Mode        g:         Opens gotop at $term -e
                                    a:         Opens calcurse using $term -e
                                    c:         Opens xcalc.
                                    Return:    Opens menu

    $mod+Shift+s    Scratch Mode    g:         Show scratchpad that matches with gotop
                                    a:         Show scratchpad that matches with calcurse
                                    c:         Show scratchpad that matches with XCalc
                                    v:         Show scratchpad that matches with VirtualBox
                                    q:         Escape mode.

    $mod+Shift+w    Window Mode     k:         Kills focused window.
                                    f:         Fullscreen.
                                    y:         Toggle floating to the focused window.
                                    h:         Split horizontally.
                                    v:         Split vertically.
                                    s:         Move window to scratchpad.
                                    Ctrl+<..>  Grows 10 pixels to the arrow side
                                    Alt+<..>   Shrinks 10 pixels to the arrow side
                                    <arrows>   Moves 20 pixels to arrow direction.
                                    q|RET      Escapes mode.

    $mode+Shift+r   Restart Mode    b:         Restart bar (calls $bar_restart)
                                    r:         Reloads i3.
                                    x:         Restart i3 and polybar (calls $x_restart)

    $mode+Shift+C   Compton Mode    f:         Flashes the active window
                                    t:         Toggle opacity on/off
                                    r:         Remove transparency
                                    s:         Set transparency
                                    i/d:       Inc/dec opacity.


For more keys, please read the config file. (I'll appending here later)

## Polybar

My polybar needs Fonts Awesome to works "beatifully", all icons comes from
there. Its colors maches with Xdefaults and Xdefaults.sepia, but it's easy
to change at the beggining of the file.

The monitor may be miss-configured... My main monitor calls HDMI-1 and
second calls eDP-1. If you differs, you must change it at the [bar/top] section.

At the modules/soft-modules are described some used modules.